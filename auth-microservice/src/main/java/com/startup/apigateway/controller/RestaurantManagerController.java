package com.startup.apigateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.mvc.ProxyExchange;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.startup.apigateway.security.Roles.ROLE_USER;

@RestController
@Secured(ROLE_USER)
public class RestaurantManagerController {

    @Value("${remote.home}")
    String home;

    @PostMapping(value = "/reserve")
    public ResponseEntity<?> reserve(ProxyExchange<byte[]> proxyExchange) {
        System.out.println("RESERVE");
        return proxyExchange.uri(home + "/reserve").post();
    }

    @PostMapping(value = "/availability")
    public ResponseEntity<?> getAvailability(
            ProxyExchange<byte[]> proxyExchange) {
        System.out.println("AVAILABILITY");
        return proxyExchange.uri(home + "/availability").post();
    }
}
