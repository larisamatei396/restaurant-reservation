const Router = require("express").Router();

const { ServerError } = require("./errors");

const { getAvailabilityForDay, reserveTable } = require("./services.js");

Router.post("/availability", async (req, res) => {
    const { date } = req.body;

    const response = await getAvailabilityForDay(date);

    if (response.error) {
        throw new ServerError(response.error, 400);
    }
    
    res.json(response.result);
});

Router.post("/reserve", async (req, res) => {
    const { date, table, name, phone, email } = req.body;

    const response = await reserveTable(date, table, name, phone, email);

    if (response.error) {
        throw new ServerError(response.error, 400);
    }

    res.json(response.result);
});

module.exports = Router;
