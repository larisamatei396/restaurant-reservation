const { sendRequest } = require("./http-client");

const getAvailabilityForDay = async (date) => {
    console.info(`Sending request to IO for availability for date ${date} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_HOST}/availability`,
        method: "POST",
        data: {
            date,
        },
    };

    const availability = await sendRequest(options);

    return availability;
};

const reserveTable = async (date, table, name, phone, email) => {
    console.info(`Sending request to IO to add reservation for ${email} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_HOST}/reserve`,
        method: "POST",
        data: {
            date,
            table,
            name,
            phone,
            email,
        },
    };

    const reservation = await sendRequest(options);

    return reservation;
};

module.exports = {
    getAvailabilityForDay,
    reserveTable,
};
