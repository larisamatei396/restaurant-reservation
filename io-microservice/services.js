const Day = require("./models/day").model;
const Reservation = require("./models/reservation").model;

const getAvailabilityForDay = async (date) => {
    console.info(`[MONGODB] Getting availability for date ${date} ...`);

    const dateTime = new Date(date);
    let returnObject = null;

    await Day.find({ date: dateTime }, async (err, docs) => {
        if (!err) {
            if (docs.length > 0) {
                // Record already exists
                returnObject = { result: docs[0] };
            } else {
                // Searched date does not exist and we need to create it
                const allTables = require("./data/allTables");
                const day = new Day({
                    date: dateTime,
                    tables: allTables,
                });
                await day.save(async (err) => {
                    if (err) {
                        returnObject = { error: "Error saving new date" };
                        // return { error: "Error saving new date" };
                    } else {
                        // Saved date and need to return all tables (because all are now available)
                        await Day.find({ date: dateTime }, (err, docs) => {
                            if (err) {
                                returnObject = { error: err };
                            } else {
                                returnObject = { result: docs[0] };
                            }
                        });
                    }
                });
            }
        } else {
            returnObject = { error: "Could not search for date" };
        }
    });

    return returnObject;
};

const reserveTable = async (date, table, name, phone, email) => {
    console.info(`[MONGODB] Adding reservation on ${date} for ${email} ...`);

    const dateTime = new Date(date);
    let returnObject = null;

    await Day.find({ date: dateTime }, async (err, days) => {
        if (!err) {
            if (days.length > 0) {
                let day = days[0];
                await day.tables.forEach(async (tableEntry) => {
                    if (tableEntry._id == table) {
                        tableEntry.reservation = new Reservation({
                            name: name,
                            phone: phone,
                            email: email,
                        });
                        tableEntry.isAvailable = false;
                        await day.save((err) => {
                            if (err) {
                                returnObject = { error: err };
                            } else {
                                returnObject = { result: "Added Reservation" };
                            }
                        });
                    }
                });
            } else {
                returnObject = { error: "Day not found" };
            }
        } else {
            returnObject = { error: "Could not search for date" };
        }
    });

    return returnObject;
};

module.exports = {
    getAvailabilityForDay,
    reserveTable,
};
