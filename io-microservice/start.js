require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const path = require("path");
const helmet = require("helmet");
const createError = require("http-errors");
const { getSecret } = require("docker-secret");
const secrets = require("./secrets");

require("express-async-errors");
require("log-timestamp");

const routes = require("./controllers.js");

// MongoDB
const mongoose = require("mongoose");
const db = mongoose.connection;

(async () => {
    // const user = secrets.read("io-user");
    // const password = secrets.read("io-pass");

    try {
        await mongoose.connect(
            `mongodb://${process.env.RESERVATIONS_DB_USER}:${process.env.RESERVATIONS_DB_PASSWORD}@${process.env.RESERVATIONS_DB_HOST}:${process.env.RESERVATIONS_DB_PORT}/${process.env.RESERVATIONS_DB_NAME}?authSource=admin`,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }
        );
    } catch (e) {
        console.trace(e);
    }
})();

const app = express();

app.use(helmet());
app.use(cors());
app.use(
    morgan(
        ':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :res[content-length]'
    )
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", routes);

app.use((err, req, res, next) => {
    console.error(err);
    let status = 500;
    let message = "Something Bad Happened";
    return next(createError(status, message));
});

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", (_) => {
    console.log("Connected to DB");
});

const port = process.env.PORT || 80;
app.listen(port, () => {
    console.log(`App is listening on ${port}`);
});
