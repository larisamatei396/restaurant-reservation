const Router = require("express").Router();

const { getAvailabilityForDay, reserveTable } = require("./services.js");

Router.post("/availability", async (req, res) => {
    const { date } = req.body;

    const response = await getAvailabilityForDay(date);

    res.json(response);
});

Router.post("/reserve", async (req, res) => {
    const { date, table, name, phone, email } = req.body;

    const response = await reserveTable(date, table, name, phone, email);

    res.json(response);
});

module.exports = Router;
