import React, { useState } from "react";
import "./App.scss";

import Main from "./components/main";
import Book from "./components/book";
import ThankYou from "./components/thankYou";
import Login from "./components/page/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

export default _ => {
  const [page, setPage] = useState(0);

  return (
    <Router>
      <Route path="/" exact component={Login} />
      <Switch>
        <Route path="/home" exact component={Main} />
        <Route path="/book" component={Book} />
        <Route path="/thanks" component={ThankYou} />
      </Switch>
    </Router>
  );
};
