import React from "react";
import { Row, Col } from "reactstrap";
import { Redirect } from "react-router";

class thanks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
        };
    }

    handleClick = () => {
        this.setState({ redirect: true });
    };

    render() {
        const { redirect } = this.state;
        return (
            <div>
                <Row noGutters className="text-center">
                    <Col>
                        <p className="thanks-header">Thank You!</p>
                        <i
                            className="fas fa-pizza-slice thank-you-pizza"
                            onClick={this.handleClick}
                        ></i>
                        {redirect && <Redirect to="/home" />}
                        <p className="thanks-subtext">
                            You should receive an email with the details of your
                            reservation.
                        </p>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default thanks;
