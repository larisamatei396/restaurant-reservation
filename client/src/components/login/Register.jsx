import React from "react";
import axios from "axios";

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRegistered: false,
        };
    }

    state = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
    };

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleSubmit = (event) => {
        event.preventDefault();

        const user = {
            email: this.state.email,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
        };

        axios
            .post(
                "http://${process.env.AUTH_SERVICE_HOST}:${process.env.AUTH_SERVICE_PORT}/register",
                user
            )
            .then((res) => {
                console.log(res.data);
            });
        this.setState({ isRegistered: true });
    };

    render() {
        const { isRegistered } = this.state;

        return (
            <div className="base-container" ref={this.props.containerRef}>
                <div className="content">
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="firstname">First name</label>
                            <input
                                type="text"
                                name="firstName"
                                placeholder="Cloud"
                                value={this.state.firstName}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="lastname">Last name</label>
                            <input
                                type="text"
                                name="lastName"
                                placeholder="Computing"
                                value={this.state.lastName}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email address</label>
                            <input
                                type="email"
                                name="email"
                                placeholder="cc.team15@gmail.com"
                                value={this.state.email}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                name="password"
                                placeholder="******"
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <button
                        type="button"
                        className="btn"
                        onClick={this.handleSubmit}
                    >
                        Register
                    </button>
                    {isRegistered && (
                        <p className="success">
                            Your account has been successfully created.
                        </p>
                    )}
                </div>
            </div>
        );
    }
}
