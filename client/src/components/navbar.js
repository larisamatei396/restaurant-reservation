import React from "react";
import { Navbar, NavbarBrand } from "reactstrap";
import { Redirect } from "react-router";

class navbar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        redirect: false,
    }
  }

  handleClick = () => {
    this.setState({redirect: true});
  }  

  render() {
    const { redirect } = this.state;
    return (
      <div>
        <Navbar light expand="md">
          <NavbarBrand
            className="nav-brand"
            onClick={this.handleClick}
          >
            Pizza Cabin
          </NavbarBrand>
          { redirect && <Redirect to="/home" /> }
        </Navbar>
      </div>
    )
  }

}

export default navbar;
