import React from "react";
import { Row, Col, Button } from "reactstrap";
import { Redirect } from "react-router";

class main extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
        redirect: false,
    }
  }

  handleClick = () => {
    this.setState({redirect: true});
  } 

  render() {
    const { redirect } = this.state;

    return (
      <div>
        <Row noGutters className="text-center align-items-center pizza-cta">
          <Col>
            <p className="looking-for-pizza">
              If you're looking for great pizza
              <i className="fas fa-pizza-slice pizza-slice"></i>
            </p>
            <Button
              color="none"
              className="book-table-btn"
              onClick={this.handleClick}
            >
              Book a Table
            </Button>
            { redirect && <Redirect to="/book" /> }
          </Col>
        </Row>
        <Row noGutters className="text-center big-img-container">
          <Col>
            <img
              src={require("../images/cafe.jpg")}
              alt="cafe"
              className="big-img"
            />
          </Col>
        </Row>
      </div>
    );
  }

}

export default main;
